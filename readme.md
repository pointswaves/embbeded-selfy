# Selfy pi

This project allows the pi to be used like a point and click camera with a low res monochromatic summary picture to give quick feed back.

This is especially useful when preparing calibration data or making training data for computer vision projects

## Hardware

The project needs a Pi, a camera module that will work with openCV and a Aida-fruit bonnet.

## Software

This is the integration project and creates a full disk image can be write directly to a sdcard.

### Build on any system

To build on any arch, set the target_arch to aarch64

```
bst -o target_arch aarch64 build deploy/image.bst
```

### Use alternate cache

Use the XDG_CACHE_HOME variable to use a alternative cache locations.

```
XDG_CACHE_HOME=/abs/path/cache; bst commands
```
